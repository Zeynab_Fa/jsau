'use strict'
let express = require('express')
let app = express('morgan')
let bodyParser = require('body-parser')
const fs = require('fs')
const morgan = require('morgan')
app.use(morgan('dev'))
app.use(express.json())


/*app.get('/', (req, res) => {
    // eslint-disable-next-line no-console
    console.log(' Hi ')
    res.end()
})*/

function getJson() {

    return new Promise((resolve, reject) => {
        fs.readFile('./src/nouvelles.json', 'utf8', (err, res) => {
            if (err) {
                reject({err: true})
            } else {
                try {
                    const obj = JSON.parse(res)
                    resolve(obj)
                } catch (err) {
                    reject({err: true})
                }
            }
        })
    })

}

app.get('/nouvelles', (req, res) => {
    fs.readFile('./src/nouvelles.json', 'utf8', (err, json) => {
        if (err) {
            res.json({err: true})
        } else {
            try {
                const obj = JSON.parse(json)
                res.json(obj)
            } catch (err) {
                res.json({err: true})
            }
        }
    })
})


/*app.get('/nouvelles/:id', (req, res) => {

  const nouvelleId = req.params.id;


  fs.readFile('./src/nouvelles.json', 'utf8', function (err, json) {
    if (err){
      res.json({err: true});
    }   else {
      try {
        const obj=JSON.parse(json);
        res.json(obj);
      } catch(err) {
        res.json({err: true});
      }
    }
  })
})

*/

// /nouvelles/add?info=blablbllblal

app.post('/nouvelles', (req, res) => {
    fs.readFile('./src/nouvelles.json', 'utf8', (err, json) => {
        if (err) {
            res.json({errr: true})
        } else {
            try {
                const obj = JSON.parse(json)

                //const id = obj['nouvelles'].length
                console.log();
                obj['nouvelles'].push(req.body);
                fs.writeFile('./src/nouvelles.json', JSON.stringify(obj), (err, results) => {
                    // eslint-disable-next-line no-console
                    console.log('added to file')
                    res.json({writeFile: true})

                })

            } catch (err) {
                // eslint-disable-next-line no-console
                console.log(error)
                //res.json({errr: true})
            }
        }
    })
})

// /nouvelles/delete/5
app.delete('/nouvelles/:id', (req, res) => {
    fs.readFile('./src/nouvelles.json', 'utf8', (err, json) => {
        if (err) {
            res.json({errr: true})
        } else {
            try {
                const obj = JSON.parse(json)
                const nId = req.params.id
                const nouvelle = obj['nouvelles'][nId] || false

                if (nouvelle) {
                    obj['nouvelles'].splice(nId, 1)
                    fs.writeFile('./src/nouvelles.json', JSON.stringify(obj), (err, results) => {
                        // eslint-disable-next-line no-console
                        console.log('deleted to file')
                        res.json({deleted: true})
                    })
                } else {
                   console.log('nouvelle intouvable')
                    return( res.json('nouvelle introuvable'))
                }

            } catch (err) {
                // eslint-disable-next-line no-console
                console.log(error)
                res.json({errr: true})
            }
        }
    })
})

// /nouvelles/update/:id?info=lklkl

app.put('/nouvelles/:id', (req, res) => {
console.log("hello");

    fs.readFile('./src/nouvelles.json', 'utf8', (err, json) => {
        if (err) {
            res.json({errr: true})
        } else {
            try {
                const obj = JSON.parse(json)
                const nId = req.params.id
                const nouvelle = obj['nouvelles'][nId] || false

                if (nouvelle) {
                    obj['nouvelles'][nId]= req.body
                    fs.writeFile('./src/nouvelles.json', JSON.stringify(obj), (err, results) => {
                        // eslint-disable-next-line no-console
                        console.log('updated file')
                      res.json()
                    })
                } else {
                  //  res.json('nouvelle introuvable')
                }

            } catch (err) {
            // eslint-disable-next-line no-console
                console.log(err)
                res.json({errr: true})
            }
        }
    })
})
/*appt.post('../JSAU/jsau-webserver/nouvelles',function (err,res){


})*/
app.listen(3000)
